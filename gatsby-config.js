module.exports = {
  siteMetadata: {
    title: `Gatsby Adde Site`,
    siteUrl: `https://www.yourdomain.tld`
  },
  plugins: [
      "gatsby-plugin-sass",
      "gatsby-plugin-image",
      "gatsby-plugin-sharp",
      {
          resolve: "gatsby-source-filesystem",
          options: {
              name: `blog`,
              path: `${__dirname}/blog`,
          }
      },
      "gatsby-plugin-mdx",
      "gatsby-transformer-sharp",
  ]
};